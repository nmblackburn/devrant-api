# devRant Api

Unoffical wrapper for the devRant API.

> This library is kept updated with the development of devRant and is the only one to document all known endpoints.

## License

This wrapper is licensed under [MIT](http://opensource.org/licenses/mit), see [LICENSE.md](LICENSE.md) for details.

## Donations

[![](https://www.buymeacoffee.com/assets/img/custom_images/black_img.png)](https://buymeacoffee.com/nblackburn)
